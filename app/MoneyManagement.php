<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class MoneyManagement extends Model
{
    protected $table = 'money_management';

    protected $fillable = ['user_id', 'balance', 'action', 'date'];

    protected $dates = ['created_at', 'updated_at'];

}
