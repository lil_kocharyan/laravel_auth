<?php

namespace App\Http\Controllers;

use App\Categories;
use App\MoneyManagement;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class MoneyManagementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

//        return response()->json([
//                'message' => $request->all()
//            ]);
        $user_id = auth('api')->user()->id;

        $data = array(
            'user_id' => $user_id,
            'balance' => $request->amount,
            'action' => $request->action,
            'date' => $request->date
        );

        $data_category = array(
            'user_id'  => $user_id,
            'category' => $request->option,
            'action' => $request->action
        );

        $check = Categories::where('category', $request->option)->get();

        if(sizeof($check) == 0){
            Categories::create($data_category);
        }
        $insert = MoneyManagement::create($data);

        if($insert) {
            return response()->json([
                'message' => 'success'
            ]);
        }
       else{
            return response()->json([
                'message' => ' something went wrong'
            ]);
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function transactions(Request $request) {
        $user_id =  auth('api')->user()->id;
        $income = DB::table('money_management')
            ->where('user_id', '=' ,$user_id)
            ->where('action', '=', 'income')->sum('balance');

        $expense = DB::table('money_management')
            ->where('user_id', '=' ,$user_id)
            ->where('action', '=', 'expense')->sum('balance');

        $total = $income - $expense;
        $all_data = MoneyManagement::where('user_id', $user_id)->orderBy('date', 'desc')->get();
        $result = [];
        foreach($all_data as $data){
            $result[$data['date']] [] = [$data['balance'],$data['action']];
        }

        return response()->json([
            'income' => $income,
            'expense' => $expense,
            'total' => $total,
            'all_data' => $result
        ]);


    }
}
